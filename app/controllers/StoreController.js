module.exports = function($scope) {

	var gems = [

		{
			name: 'Dodecahedron',
			price: 2.95,
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
			canPurchase: true,
			soldOut: false,
			reviews: [
				{
					stars: 5,
					body: "I love this product!",
					author: "joe@thomas.com"
				},
				{
					stars: 1,
					body: "This product sucks!",
					author: "tim@hater.com"
				}
			]
		},
		{
			name: 'Pentagon',
			price: 5.95,
			desc: 'Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
			canPurchase: true,
			soldOut: false,
			reviews: [
				{
					stars: 1,
					body: "This product sucks!",
					author: "tim@hater.com"
				},
				{
					stars: 5,
					body: "I love this product!",
					author: "joe@thomas.com"
				}
			]
		}
	]

	this.products = gems;
}