require('angular')
var StoreController = require('./controllers/StoreController')
var PanelController = require('./controllers/PanelController')
var ReviewController = require('./controllers/ReviewController')

var app = angular.module('store', ['store-products'])

app.controller('StoreController', ['$scope', StoreController])
app.controller('ReviewController', ['$scope', ReviewController])
app.controller('PanelController', ['$scope', PanelController])

