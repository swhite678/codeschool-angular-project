var gulp = require('gulp')
var sass = require('gulp-ruby-sass')
var connect = require('gulp-connect')
var browserify = require('browserify')
var source = require('vinyl-source-stream')
var watch = require('gulp-watch')
var batch = require('gulp-batch')

gulp.task('connect', function () {
	connect.server({
		root: './',
		port: 4000
	})
})

gulp.task('browserify', function() {
	// Grabs the app.js file
	return browserify('./app/app.js')
	// bundles it and creates a file called main.js
	.bundle()
	.pipe(source('main.js'))
	// saves it the ./js/ directory
	.pipe(gulp.dest('./js/'));
})

gulp.task('sass', function() {
	return sass('scss/style.scss')
	.pipe(gulp.dest('css'))
})

gulp.task('stream', function () {
	return gulp.src('./app/**/*.js')
		.pipe(watch('./app/**/*.js'))
		.pipe(gulp.dest('build'));
});

gulp.task('watch', function () {

	watch('./app/**/*.js', batch(function (events, done) {
		gulp.start('browserify', done);
	}));

	watch('./scss/style.scss', batch(function (events, done) {
		gulp.start('sass', done);
	}));
});

// gulp.task('watch', function() {
// 	gulp.watch('./app/**/*.js', ['browserify'])
// 	gulp.watch('./scss/style.scss', ['sass'])
// })

gulp.task('default', ['connect', 'watch'])